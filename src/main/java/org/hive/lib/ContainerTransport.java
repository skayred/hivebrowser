package org.hive.lib;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;

import javax.xml.namespace.QName;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 9/16/13
 * Time: 4:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class ContainerTransport {
    private Client client;

    public ContainerTransport(String host, int port) {
        JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();

        client = dcf.createClient("http://" + host + ":" + port + "/container?wsdl");
    }

    public String getInterface(String applicationName, String interfaceName) throws Exception {
        Object[] res = client.invoke(new QName("http://proxy.container.hive.org/", "showInterface"), applicationName, interfaceName);

        return (String) res[0];
    }

    public String getClassesDefinitions(String applicationName) throws Exception {
        Object[] res = client.invoke(new QName("http://proxy.container.hive.org/", "getClassesDefinitions"), applicationName);

        return (String) res[0];
    }

    public Object processRequest(String applicationName, String methodName, List<Object> args) throws Exception {
        Object[] res = client.invoke(new QName("http://proxy.container.hive.org/", "processRequest"), applicationName, methodName, args);

        return res[0];
    }
}