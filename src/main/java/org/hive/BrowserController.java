package org.hive;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import org.eclipse.persistence.jaxb.dynamic.DynamicJAXBContext;
import org.eclipse.persistence.jaxb.dynamic.DynamicJAXBContextFactory;
import org.hive.lib.ContainerTransport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sound.midi.ControllerEventListener;
import javax.xml.bind.JAXBException;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;

public class BrowserController {
    private static final Logger log = LoggerFactory.getLogger(BrowserController.class);

    @FXML private TextField urlField;
    @FXML private Pane contentPane;
    @FXML private ProgressIndicator progressIndicator;

    private FXMLLoader loader;

    public void initialize() {
        loader = new FXMLLoader();

        urlField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    loadPage(urlField.getText());
                }
            }
        });
    }

    private void loadPage(final String url) {
        progressIndicator.setVisible(true);

        Thread queryThread = new Thread() {
            public void run() {
                Parent rootNode = null;

                //  TODO: use logger from Library Project
                try {
                    ContainerTransport transport = getTransport(url);

                    File pageContent = getPageContent(transport, url);

                    //  TODO: dynamically generate classes, compile them and load at the runtime
                    //  TODO: move to other class and create a register for loaded contexts
                    File typesContent = getClassesDefinition(transport, url);

                    FileInputStream xsdInputStream = new FileInputStream(typesContent);
                    DynamicJAXBContext jaxbContext = DynamicJAXBContextFactory.createContextFromXSD(xsdInputStream, null, null, null);

                    rootNode = (Parent) loader.load(new FileInputStream(pageContent));
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                } catch (JAXBException e) {
                    e.printStackTrace();
                }

                final Parent finalRootNode = rootNode;
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        progressIndicator.setVisible(false);
                        contentPane.getChildren().add(finalRootNode);
                    }
                });
            }
        };
        queryThread.start();
    }

    private ContainerTransport getTransport(String url) throws URISyntaxException {
        URI requestUrl = new URI(url);

        return new ContainerTransport(requestUrl.getHost(), requestUrl.getPort());
    }

    //  Gets page to temp file
    private File getPageContent(ContainerTransport transport, String url) throws IOException {
        File temp = File.createTempFile("tempfile", ".tmp");

        try {
            URI requestUrl = new URI(url);

            String[] pathSegments = requestUrl.getPath().split("/");

            BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
            bw.write(transport.getInterface(pathSegments[1], pathSegments[2]));
            bw.close();

            return temp;
        } catch (URISyntaxException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return null;
    }

    //  Gets page to temp file
    private File getClassesDefinition(ContainerTransport transport, String url) throws IOException {
        File temp = File.createTempFile("types-tempfile", ".xsd");

        try {
            URI requestUrl = new URI(url);

            String[] pathSegments = requestUrl.getPath().split("/");

            BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
            bw.write(transport.getClassesDefinitions(pathSegments[1]));
            bw.close();

            return temp;
        } catch (URISyntaxException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return null;
    }
}
