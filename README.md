Before first using you must fix your classpath:

    mvn com.zenjava:javafx-maven-plugin:2.0:fix-classpath

Building:

    mvn clean install

Running:

    mvn jfx:run
